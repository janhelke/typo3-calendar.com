<?php
$EM_CONF['sitepackage'] = [
    'title' => 'Template typo3-calendar.com',
    'description' => 'Sitepackage for typo3-calendar.com',
    'category' => 'template',
    'author' => 'Jan Helke',
    'author_email' => 'info@typo3-calendar.com',
    'state' => 'stable',
    'clearCacheOnLoad' => 1,
    'lockType' => '',
    'author_company' => 'quintanion.net',
    'version' => '1.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '10.99.99'
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
