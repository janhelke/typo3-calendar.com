<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

/***************
 * Register custom EXT:form configuration
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScriptSetup(trim('
    module.tx_form {
        settings {
            yamlConfigurations {
                1549657423 = EXT:sitepackage/Configuration/Form/Setup.yaml
            }
        }
    }
    plugin.tx_form {
        settings {
            yamlConfigurations {
                1549657423 = EXT:sitepackage/Configuration/Form/Setup.yaml
            }
        }
    }
'));
