#!/usr/bin/env bash

TIMESTAMP=$(date +%Y%m%d%H%M%S)
echo "${CI_COMMIT_REF_SLUG}"
STAGE_PATH=${SSH_PATH}/${CI_COMMIT_REF_SLUG}
echo "${STAGE_PATH}"
RELEASE=${CI_PIPELINE_ID}_${TIMESTAMP}
RELEASE_PATH=${STAGE_PATH}/releases/${RELEASE}

echo ">>>>> Create Release: $RELEASE <<<<<"
ssh -o StrictHostKeyChecking=no -p 22 ${SSH_CONNECTION} "mkdir -p $RELEASE_PATH && mkdir -p $STAGE_PATH/shared/fileadmin"
retval=$?

if [ $retval -eq 0 ]; then
  echo ">>>>> Rsync files <<<<<"
  rsync -rav -e "ssh -p 22" --include-from=Build/Deployment/filestructure.rules . ${SSH_CONNECTION}:${RELEASE_PATH}/
  retval=$((retval + $?))
fi

if [ $retval -eq 0 ]; then
  echo ">>>>> Setup TYPO3 <<<<<"
  ssh -o StrictHostKeyChecking=no -p 22 ${SSH_CONNECTION} "
  ln -sr $STAGE_PATH/shared/fileadmin $RELEASE_PATH/Web/ && \
  ln -sr $STAGE_PATH/shared/.env $RELEASE_PATH/ && \
  cd $RELEASE_PATH && \
  $SSH_PHP vendor/bin/typo3cms install:generatepackagestates && \
  $SSH_PHP vendor/bin/typo3cms install:fixfolderstructure && \
  $SSH_PHP vendor/bin/typo3cms database:updateschema && \
  $SSH_PHP vendor/bin/typo3cms cache:flush && \
  $SSH_PHP vendor/bin/typo3cms language:update"
  retval=$((retval + $?))
fi

if [ $retval -eq 0 ]; then
  echo ">>>>> Release & typo3cms binary link <<<<<"
  ssh -o StrictHostKeyChecking=no -p 22 ${SSH_CONNECTION} "
  ln -fsn $RELEASE_PATH/Web $STAGE_PATH/current && \
  ln -fsn $RELEASE_PATH/vendor/bin/typo3cms $STAGE_PATH/typo3cms"
  retval=$((retval + $?))
fi

if [ $retval -eq 0 ]; then
  echo ">>>>> Remove old releases <<<<<"
  ssh -o StrictHostKeyChecking=no -p 22 ${SSH_CONNECTION} "
  cd $STAGE_PATH/releases/ && find -maxdepth 1 -type d ! -iname \"$RELEASE\" -exec rm -rf {} \;"
  warning=$((warning + $?))

  echo ">>>>> Cronjob path: $STAGE_PATH/typo3cms <<<<<"
  echo ">>>>> Deployment: $SSH_CONNECTION:$RELEASE_PATH <<<<<"
fi

exit $retval
